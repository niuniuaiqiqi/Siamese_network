import os
import time
import sys

# 写入
def input_write(path,data_string):
    print("写入数据流",data_string)
    with open(path,'a+',encoding='utf-8') as f:
        f.write(data_string)

# 读取
def output_read(path):
    with open(path,'r',encoding="utf-8") as f:
        data_string = f.read()
    print(data_string)

# 判断 是否存在文件目录
def judge_dir(path):
    if  not os.path.exists(path):
        os.mkdir(path)

if __name__ == "__main__":
    path_dir = r"./log"
    judge_dir(path_dir)
    path = r"./log/log.txt"
    data = input("请输入 你想写入的文本")
    input_write(path,data)
    time.sleep(5)
    print("输出文本流")
    output_read(path)

